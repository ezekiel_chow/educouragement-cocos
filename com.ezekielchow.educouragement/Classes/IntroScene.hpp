//
//  IntroScene.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 17/03/2017.
//
//

#ifndef IntroScene_hpp
#define IntroScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class IntroScene : public cocos2d::Layer
{
private:
   
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(IntroScene);
};

#endif /* IntroScene_hpp */
