//
//  ToUpdateSingleton.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 20/03/2017.
//
//

#ifndef ToUpdateSingleton_hpp
#define ToUpdateSingleton_hpp

#include <stdio.h>
#include <iostream>

struct ToUpdateSingleton
{
    // public interface
    //settres
    virtual void setStrength(int strength);
    virtual void setIntelligence(int intelligence);
    virtual void setLevel(int level);
    virtual void setExperience(int experience);
    virtual void setGold(int gold);
    
    //geters
    virtual int getStrength();
    virtual int getIntelligence();
    virtual int getLevel();
    virtual int getExperience();
    virtual int getGold();
private:
    struct impl;
    static impl& get_impl();
    
};

#endif /* ToUpdateSingleton_hpp */
