//
//  MultiplayerGame.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 11/03/2017.
//
//

#include "MultiplayerGame.hpp"
#include <iostream>

#define COCOS2D_DEBUG 1

USING_NS_CC;
using namespace std;
using namespace cocos2d::network;
Scene* MultiplayerGame::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MultiplayerGame::create();
    
    // add layer as a child to scene
    scene->addChild(layer,1);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MultiplayerGame::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(135,206,250,255)) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    mySingleton = PlayerDataSingleton();
    cout<<"did get roomanem?"<<mySingleton.getRoomName();
    
    //draw backarrow
    auto backButton = cocos2d::ui::Button::create("images/backarrow.png","", "");
    backButton->setPosition(Vec2(visibleSize.width*0.05 + origin.x, visibleSize.height * 0.95 + origin.y));
    backButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->popScene();
                break;
            default:
                break;
        }
    });
    this->addChild(backButton);
    
    auto answerButton1 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png", "images/AnswerButtonAfter.png", "");
    answerButton1->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.85 + origin.y));
    answerButton1->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                break;
            default:
                break;
        }
    });
    this->addChild(answerButton1, 10);
    
    auto answerButton2 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png", "images/AnswerButtonAfter.png", "");
    answerButton2->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.75 + origin.y));
    answerButton2->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                break;
            default:
                break;
        }
    });
    this->addChild(answerButton2, 10);
    
    auto answerButton3 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png", "images/AnswerButtonAfter.png", "");
    answerButton3->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.15 + origin.y));
    answerButton3->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                break;
            default:
                break;
        }
    });
    this->addChild(answerButton3, 10);
    
    auto answerButton4 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png", "images/AnswerButtonAfter.png", "");
    answerButton4->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.05 + origin.y));
    answerButton4->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                break;
            default:
                break;
        }
    });
    this->addChild(answerButton4, 10);
    
    auto infoLabel = Label::createWithTTF("question" , "fonts/arial.ttf", 12);
    infoLabel->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(infoLabel, 10);
    
    cocos2d::network::SIOClient *client = SocketIO::connect("https://educouragement-gameroom.herokuapp.com:14732", *this);
    
//    client->emit("checkConnection", "[{\"name\":\"ezekiel\"}]");
//    client->on("connectedToRoom", CC_CALLBACK_2(MultiplayerGame::connectedToRoom, this));
    return true;
}

void MultiplayerGame::connectedToRoom(cocos2d::network::SIOClient *client, const std::string& data)
{
    cout<<"\nconnected to room:"<<data;
}

void MultiplayerGame::onConnect(SIOClient* client) { CCLOG("SIODelegate onConnect fired"); };
/**
 * This is kept for backwards compatibility, message is now fired as a socket.io event "message"
 *
 * This function would be called when the related SIOClient object receive message or json message.
 *
 * @param client the connected SIOClient object.
 * @param data the message,it could be json message
 */
void MultiplayerGame::onMessage(SIOClient* client, const std::string& data) { CCLOG("SIODelegate onMessage fired with data: %s", data.c_str()); };
/**
 * Pure virtual callback function, this function should be overrided by the subclass.
 *
 * This function would be called when the related SIOClient object disconnect or receive disconnect signal.
 *
 * @param client the connected SIOClient object.
 */
void MultiplayerGame::onClose(SIOClient* client)
{
    
}
/**
 * Pure virtual callback function, this function should be overrided by the subclass.
 *
 * This function would be called when the related SIOClient object receive error signal or didn't connect the endpoint but do some network operation, eg.,send and emit,etc.
 *
 * @param client the connected SIOClient object.
 * @param data the error message
 */
void MultiplayerGame::onError(SIOClient* client, const std::string& data)
{
    
}
/**
 * Fire event to script when the related SIOClient object receive the fire event signal.
 *
 * @param client the connected SIOClient object.
 * @param eventName the event's name.
 * @param data the event's data information.
 */
void MultiplayerGame::fireEventToScript(SIOClient* client, const std::string& eventName, const std::string& data) { CCLOG("SIODelegate event '%s' fired with data: %s", eventName.c_str(), data.c_str()); };
