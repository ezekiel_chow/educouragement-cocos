//
//  Settings.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 28/03/2017.
//
//

#include "FacebookLogin.hpp"
#include "MainMenu.hpp"
#include "Settings.hpp"
USING_NS_CC;

Scene* Settings::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Settings::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Settings::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B::GRAY) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    sdkbox::PluginFacebook::setListener(this);
    
    //back button
    auto backBtn = cocos2d::ui::Button::create("images/backarrowWhite.png", "images/backarrowWhite.png", "");
    backBtn->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.95 + origin.y));
    backBtn->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->reorderChild(backBtn, 10);
    this->addChild(backBtn);
    
    //back button
    auto logout = cocos2d::ui::Button::create("images/LogoutButton.png", "images/LogoutButtonAfter.png", "");
    logout->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.5 + origin.y));
    logout->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                sdkbox::PluginFacebook::logout();
                Director::getInstance()->replaceScene(FacebookLogin::createScene());
                break;
            default:
                break;
        }
    });
    this->reorderChild(logout, 10);
    this->addChild(logout);
    
    return true;
}

//Facebook methods
void Settings::onLogin(bool isLogin, const std::string &msg)
{
    if (isLogin==true) {
        std::cout<<"logout? succeed"+msg;
    }
    else{
        std::cout<<"logout? fail";
    }
}

void Settings::onPermission(bool isLogin, const std::string &msg)
{}

void Settings::onAPI(const std::string &tag, const std::string &jsonData)
{}

void Settings::onSharedSuccess(const std::string &message)
{}

void Settings::onSharedCancel()
{}

void Settings::onSharedFailed(const std::string &message)
{}

void Settings::onFetchFriends(bool ok, const std::string &msg)
{}

void Settings::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo &friends)
{}

void Settings::onInviteFriendsWithInviteIdsResult(bool result, const std::string &msg)
{}

void Settings::onInviteFriendsResult(bool result, const std::string &msg)
{}

void Settings::onGetUserInfo(const sdkbox::FBGraphUser &userInfo)
{}
