//
//  EndGame.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 20/03/2017.
//
//

#include "MainMenu.hpp"
#include "EndGame.hpp"
USING_NS_CC;

Scene* EndGame::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = EndGame::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool EndGame::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    mySingleton = PlayerDataSingleton();
    sdkbox::PluginFacebook::setListener(this);
    
    Sprite * space = Sprite::create("images/Space.jpg");
    Size wSize = Size(Director::getInstance()->getWinSizeInPixels().width, Director::getInstance()->getWinSizeInPixels().height);
    Size scrSize= Size(1136, 640);
    space->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    space->setScaleX(wSize.width/scrSize.width);
    space->setScaleY(wSize.height/scrSize.height);
    this->addChild(space);
    this->reorderChild(space, -1);
    
    updateQuestData();
    drawNodes();
    
    return true;
}

void EndGame::drawNodes()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    std::string completeText = "";
    int goldToGive, expToGive;
    Color4B textColor;
    if (mySingleton.getMissionSucceed())
    {
        completeText = "Mission Success!";
        goldToGive = 150;
        expToGive = 80;
        mySingleton.setGold(mySingleton.getGold() + goldToGive);
        mySingleton.setExperience(mySingleton.getExperience() + expToGive);
    }
    else
    {
        goldToGive = 5 * mySingleton.getCorrectTotal();
        expToGive = 2 * mySingleton.getCorrectTotal();
        completeText = "Mission Fail!";
        mySingleton.setGold(mySingleton.getGold() + goldToGive);
        mySingleton.setExperience(mySingleton.getExperience() + expToGive);
    }
    
    auto winloseLbl = ui::Text::create(completeText, "fonts/arial.ttf", 25);
    winloseLbl->setTextHorizontalAlignment(TextHAlignment::CENTER);
    winloseLbl->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.85 + origin.y));
    winloseLbl->setTextColor(Color4B::MAGENTA);
    this->addChild(winloseLbl);
    this->reorderChild(winloseLbl, 5);
    
    auto correctTotal = Label::createWithTTF(std::to_string(mySingleton.getCorrectTotal()) , "fonts/arial.ttf", 40);
    correctTotal->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.65 + origin.y));
    correctTotal->setTextColor(Color4B::BLUE);
    this->addChild(correctTotal);
    this->reorderChild(correctTotal, 5);
    
    auto wrongTotal = Label::createWithTTF(std::to_string(mySingleton.getWrongTotal()), "fonts/arial.ttf", 40);
    wrongTotal->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.65 + origin.y));
    wrongTotal->setTextColor(Color4B::RED);
    this->reorderChild(wrongTotal, 5);
    this->addChild(wrongTotal);
    
    auto correctLbl = Label::createWithTTF("Correct" , "fonts/arial.ttf", 18);
    correctLbl->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.55 + origin.y));
    correctLbl->setTextColor(Color4B::BLUE);
    this->addChild(correctLbl);
    this->reorderChild(correctLbl, 5);
    
    auto wrongLbl = Label::createWithTTF("Incorrect" , "fonts/arial.ttf", 18);
    wrongLbl->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.55 + origin.y));
    wrongLbl->setTextColor(Color4B::RED);
    this->addChild(wrongLbl);
    this->reorderChild(wrongLbl, 5);
    

    auto goldTotal = Label::createWithTTF("+" + std::to_string(goldToGive), "fonts/arial.ttf", 25);
    goldTotal->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.40 + origin.y));
    Color4B goldColor = Color4B(255, 215, 0, 255);
    goldTotal->setTextColor(goldColor);
    this->addChild(goldTotal);
    this->reorderChild(goldTotal, 5);
    
    auto expTotal = Label::createWithTTF("+" + std::to_string(expToGive), "fonts/arial.ttf", 25);
    expTotal->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.40 + origin.y));
    expTotal->setTextColor(Color4B::WHITE);
    this->reorderChild(expTotal, 5);
    this->addChild(expTotal);
    
    auto *goldLbl = Sprite::create("images/coins.png");
    goldLbl->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.30 + origin.y));
    this->addChild(goldLbl);
    this->reorderChild(goldLbl, 5);
    
    auto expLbl = Label::createWithTTF("EXP", "fonts/arial.ttf", 25);
    expLbl->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.30 + origin.y));
    expLbl->setTextColor(Color4B::WHITE);
    this->reorderChild(expLbl, 5);
    this->addChild(expLbl);
    
    continueButton = cocos2d::ui::Button::create("images/ContinueButtonBefore.png","images/ContinueButtonAfter.png", "images/LoadingBefore.png");
    continueButton->setPosition(Vec2(visibleSize.width*0.45 + origin.x, visibleSize.height*0.15 + origin.y));
    continueButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
                
        }
    });
    continueButton->setEnabled(false);
    this->addChild(continueButton);
    this->reorderChild(continueButton, 5);
    
    auto shareButton = cocos2d::ui::Button::create("images/facebookShare.png","images/facebookShare.png", "");
    shareButton->setPosition(Vec2(visibleSize.width*0.9 + origin.x, visibleSize.height*0.15 + origin.y));
    shareButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                sdkbox::FBShareInfo info;
                info.type  = sdkbox::FB_LINK;
                info.link  = "https://www.facebook.com/EduCouragement-245985525864586/";
                info.title = "EduCouragement";
                info.text  = "Correct:" + std::to_string(mySingleton.getCorrectTotal()) + "\nWrong:" + std::to_string(mySingleton.getWrongTotal());
                info.image = "https://www.facebook.com/245985525864586/photos/a.245989112530894.1073741826.245985525864586/245989115864227/?type=1&theater";
                sdkbox::PluginFacebook::dialog(info);
                break;
        }
    });
    this->addChild(shareButton);
    this->reorderChild(shareButton, 5);
}

void EndGame::updateQuestData()
{
    cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
    request->setUrl("https://educouragement-rest.herokuapp.com/api/quest");
    request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
    request->setResponseCallback( CC_CALLBACK_2(EndGame::updateSuccess, this) );
    
    // write the post data
    std::string winloseTxt;
    if (mySingleton.getMissionSucceed())
    {
        winloseTxt = "win";
    }
    else
    {
        winloseTxt = "lose";
    }
    
    std::string postDataStr = "userid=" + sdkbox::PluginFacebook::getUserID()  + "&quest=" + std::to_string(mySingleton.getQuestSelected()) + "&correct_total=" + std::to_string(mySingleton.getCorrectTotal())  + "&wrong_total=" + std::to_string(mySingleton.getWrongTotal())  + "&winlose=" + winloseTxt;
    std::cout<<"\ndata:"<<postDataStr;
    const char *postData = postDataStr.c_str();
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("POST quest");
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
}

void EndGame::updateSuccess(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    std::cout<<"\ndid come in ah";
    if (!response)
    {
        log("onHttpRequestCompleted - No Response");
        return;
    }
    
    if (!response->isSucceed())
    {
        log("onHttpRequestCompleted - Response failed");
        log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    char * concatenated = (char *) malloc(buffer->size() + 1);
    std::string s2(buffer->begin(), buffer->end());
    strcpy(concatenated, s2.c_str());
    
    continueButton->setEnabled(true);
}

void EndGame::drawDialog(std::string message)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto infoLabel = Label::createWithTTF(message , "fonts/arial.ttf", 12);
    infoLabel->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.95 + origin.y));
    infoLabel->setTag(77);
    this->addChild(infoLabel, 10);
}

//Facebook methods
void EndGame::onLogin(bool isLogin, const std::string &msg)
{
}

void EndGame::onPermission(bool isLogin, const std::string &msg)
{}

void EndGame::onAPI(const std::string &tag, const std::string &jsonData)
{}

void EndGame::onSharedSuccess(const std::string &message)
{
    drawDialog("successfully shared");
}

void EndGame::onSharedCancel()
{
    drawDialog("sharing cancelled");
}

void EndGame::onSharedFailed(const std::string &message)
{
    drawDialog("sharing failed");
}

void EndGame::onFetchFriends(bool ok, const std::string &msg)
{}

void EndGame::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo &friends)
{}

void EndGame::onInviteFriendsWithInviteIdsResult(bool result, const std::string &msg)
{}

void EndGame::onInviteFriendsResult(bool result, const std::string &msg)
{}

void EndGame::onGetUserInfo(const sdkbox::FBGraphUser &userInfo)
{}
