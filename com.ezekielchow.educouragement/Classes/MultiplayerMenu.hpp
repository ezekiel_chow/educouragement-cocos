//
//  MultiplayerMenu.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 09/03/2017.
//
//

#ifndef MultiplayerMenu_hpp
#define MultiplayerMenu_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "PluginFacebook/PluginFacebook.h"
#include "network/HttpClient.h"
#include "PlayerDataSingleton.hpp"
#include "ui/CocosGUI.h"

class MultiplayerMenu : public cocos2d::LayerColor
{
private:
    
    std::string roomName;
    PlayerDataSingleton mySingleton;
    
    void textFieldUsed(Ref * pSender, cocos2d::ui::TextField::EventType type);
    
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(MultiplayerMenu);
};

#endif /* MultiplayerMenu_hpp */
