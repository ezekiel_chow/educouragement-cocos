//
//  QuestionsModel.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 13/03/2017.
//
//

#include "QuestionsModel.hpp"

void QuestionSet::init(std::string question, std::string answerone, std::string answertwo, std::string answerthree, std::string answerfour, int correctanswer)
{
    questionR = question;
    answeroneR = answerone;
    answertwoR = answertwo;
    answerthreeR = answerthree;
    answerfourR = answerfour;
    correctanswerR = correctanswer;
}

std::string QuestionSet::getQuestion()
{
    return questionR;
}
std::string QuestionSet::getAnswerOne()
{
    return answeroneR;
}
std::string QuestionSet::getAnswerTwo()
{
    return answertwoR;
}
std::string QuestionSet::getAnswerThree()
{
    return answerthreeR;
}
std::string QuestionSet::getAnswerFour()
{
    return answerfourR;
}
int QuestionSet::getCorrectAnswer()
{
    return correctanswerR;
}

void QuestionSet::setQuestion(std::string question)
{
    questionR = question;
}
void QuestionSet::setAnswerOne(std::string answerone)
{
    answeroneR = answerone;
}
void QuestionSet::setAnswerTwo(std::string answertwo)
{
    answertwoR = answertwo;
}
void QuestionSet::setAnswerThree(std::string answerthree)
{
    answerthreeR = answerthree;
}
void QuestionSet::setAnswerFour(std::string answerfour)
{
    answerfourR = answerfour;
}
void QuestionSet::setCorrectAnswer(int correctanswer)
{
    correctanswerR = correctanswer;
}
