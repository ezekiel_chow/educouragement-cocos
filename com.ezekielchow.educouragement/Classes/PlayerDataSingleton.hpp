//
//  PlayerDataSingleton.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 10/03/2017.
//
//

#ifndef PlayerDataSingleton_hpp
#define PlayerDataSingleton_hpp

#include <stdio.h>
#include <iostream>

struct PlayerDataSingleton
{
    // public interface
    //settres
    virtual void setTeam(std::string team);
    virtual void setStrength(int strength);
    virtual void setIntelligence(int intelligence);
    virtual void setStress(int stress);
    virtual void setSocial(int social);
    virtual void setLevel(int level);
    virtual void setExperience(int experience);
    virtual void setGold(int gold);
    virtual void setRoomName(std::string roomName);
    virtual void setQuestSelected(int questSelected);
    virtual void setCorrectTotal(int correctTotal);
    virtual void setWrongTotal(int wrongTotal);
    virtual void setMissonSucceed(bool didSucceed);
    virtual void setItems(std::vector<int> items);
    virtual void setDidWin(bool didWin);
    
    //geters
    virtual std::string getTeam();
    virtual int getStrength();
    virtual int getIntelligence();
    virtual int getStress();
    virtual int getSocial();
    virtual int getLevel();
    virtual int getExperience();
    virtual int getGold();
    virtual std::string getRoomName();
    virtual int getQuestSelected();
    virtual int getCorrectTotal();
    virtual int getWrongTotal();
    virtual bool getMissionSucceed();
    virtual std::vector<int> getItems();
    virtual bool getDidWin();
    
private:
    struct impl;
    static impl& get_impl();
    
};

#endif /* PlayerDataSingleton_hpp */
