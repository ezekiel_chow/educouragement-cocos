//
//  MultiplayerGame.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 11/03/2017.
//
//

#ifndef MultiplayerGame_hpp
#define MultiplayerGame_hpp

#include <stdio.h>
#include "PlayerDataSingleton.hpp"
#include "network/SocketIO.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class MultiplayerGame : public cocos2d::LayerColor, cocos2d::network::SocketIO::SIODelegate
{
private:
    
    PlayerDataSingleton mySingleton;
    
    //socket io funcs
    virtual void onConnect(cocos2d::network::SIOClient* client);
    virtual void onMessage(cocos2d::network::SIOClient* client, const std::string& data);
    virtual void onClose(cocos2d::network::SIOClient* client);
    virtual void onError(cocos2d::network::SIOClient* client, const std::string& data);
    virtual void fireEventToScript(cocos2d::network::SIOClient* client, const std::string& eventName, const std::string& data);
    
    void connectedToRoom(cocos2d::network::SIOClient *client, const std::string& data);

public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(MultiplayerGame);
};

#endif /* MultiplayerGame_hpp */
