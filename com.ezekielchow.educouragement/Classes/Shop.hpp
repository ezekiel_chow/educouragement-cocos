//
//  Shop.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 20/03/2017.
//
//

#ifndef Shop_hpp
#define Shop_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PlayerDataSingleton.hpp"

class Shop : public cocos2d::LayerColor
{
private:
    PlayerDataSingleton mySingleton;
    cocos2d::Label *gold;
    
    virtual void updatePlayerAttribute();
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(Shop);
};

#endif /* Shop_hpp */
