//
//  Shop.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 20/03/2017.
//
//

#include "MainMenu.hpp"
#include "Shop.hpp"
USING_NS_CC;

Scene* Shop::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Shop::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Shop::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B::GRAY) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    mySingleton = PlayerDataSingleton();
    
    //back button
    auto backBtn = cocos2d::ui::Button::create("images/backarrowWhite.png", "images/backarrowWhite.png", "");
    backBtn->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.95 + origin.y));
    backBtn->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->reorderChild(backBtn, 10);
    this->addChild(backBtn);
    
    auto sceneTitle = Label::createWithTTF("Shop" , "fonts/arial.ttf", 14);
    sceneTitle->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.95 + origin.y));
    this->addChild(sceneTitle);
    
    auto *goldLbl = Sprite::create("images/coins.png");
    goldLbl->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.1 + origin.y));
    this->addChild(goldLbl);
    this->reorderChild(goldLbl, 5);
    
    gold = Label::createWithTTF(std::to_string(mySingleton.getGold()), "fonts/arial.ttf", 10);
    gold->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.1 + origin.y));
    gold->setTextColor(Color4B::BLACK);
    this->reorderChild(gold, 5);
    this->addChild(gold);
    
    auto *rocket = Sprite::create("images/ShopRocket.png");
    rocket->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.75 + origin.y));
    this->addChild(rocket);
    this->reorderChild(rocket, 5);
    
    auto *oxygenTank = Sprite::create("images/ShopOxygenTank.png");
    oxygenTank->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.75 + origin.y));
    this->addChild(oxygenTank);
    this->reorderChild(oxygenTank, 5);
    
    auto *rocketCost = Label::createWithTTF("Gold -65", "fonts/arial.ttf", 10);
    rocketCost->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.65 + origin.y));
    rocketCost->setTextColor(Color4B::BLACK);
    this->reorderChild(rocketCost, 5);
    this->addChild(rocketCost);
    
    auto *oxygenTankCost = Label::createWithTTF("Gold -30", "fonts/arial.ttf", 10);
    oxygenTankCost->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.65 + origin.y));
    oxygenTankCost->setTextColor(Color4B::BLACK);
    this->reorderChild(oxygenTankCost, 5);
    this->addChild(oxygenTankCost);
    
    auto *rocketDescription = Label::createWithTTF("Oxygen Tank +1", "fonts/arial.ttf", 10);
    rocketDescription->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.55 + origin.y));
    rocketDescription->setTextColor(Color4B::BLACK);
    this->reorderChild(rocketDescription, 5);
    this->addChild(rocketDescription);
    
    auto *oxygenTankDescription = Label::createWithTTF("Tank Size +3", "fonts/arial.ttf", 10);
    oxygenTankDescription->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.55 + origin.y));
    oxygenTankDescription->setTextColor(Color4B::BLACK);
    this->reorderChild(oxygenTankDescription, 5);
    this->addChild(oxygenTankDescription);
    
    auto rocketBuy = cocos2d::ui::Button::create("images/BuyBefore.png","images/BuyAfter.png", "images/BuyDisable.png");
    rocketBuy->setPosition(Vec2(visibleSize.width*0.25 + origin.x, visibleSize.height*0.50 + origin.y));
    rocketBuy->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                mySingleton.setGold(mySingleton.getGold()-65);
                mySingleton.setStrength(mySingleton.getStrength() + 1);
                gold->setString(std::to_string(mySingleton.getGold()));
                break;
            default:
                break;
                
        }
    });
    this->addChild(rocketBuy);
    this->reorderChild(rocketBuy, 5);
    
    auto oxygenTankBuy = cocos2d::ui::Button::create("images/BuyBefore.png","images/BuyAfter.png", "images/BuyDisable.png");
    oxygenTankBuy->setPosition(Vec2(visibleSize.width*0.75 + origin.x, visibleSize.height*0.50 + origin.y));
    oxygenTankBuy->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                mySingleton.setGold(mySingleton.getGold()-30);
                mySingleton.setIntelligence(mySingleton.getIntelligence()+3);
                gold->setString(std::to_string(mySingleton.getGold()));
                break;
            default:
                break;
                
        }
    });
    this->addChild(oxygenTankBuy);
    this->reorderChild(oxygenTankBuy, 5);
    
    std::vector<int> myItems = mySingleton.getItems();
    for (int i = 0; i< myItems.size(); i++) {
        if (myItems[i] == 1)
        {
            rocketBuy->setEnabled(false);
        }
        else if(myItems[i] == 2)
        {
            oxygenTankBuy->setEnabled(false);
        }
    }
    
    return true;
}

void Shop::updatePlayerAttribute()
{
    
}

