//
//  MultiplayerMenu.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 09/03/2017.
//
//

#include "MainMenu.hpp"
#include "MultiplayerMenu.hpp"
#include <iostream>
#include "external/rapidjson/reader.h"
#include "external/rapidjson/writer.h"
#include "external/rapidjson/document.h"
#include "external/rapidjson/stringbuffer.h"
#include "MultiplayerGame.hpp"

#define COCOS2D_DEBUG 1

USING_NS_CC;
using namespace std;
using namespace rapidjson;

Scene* MultiplayerMenu::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MultiplayerMenu::create();
    
    // add layer as a child to scene
    scene->addChild(layer,1);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MultiplayerMenu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(135,206,250,255)) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    mySingleton = PlayerDataSingleton();
    
    //initialise data avoid error
    roomName = "name please";
    mySingleton.setRoomName("name please");

    cout<<"mysingleton data: "<<mySingleton.getTeam()<<" "<<mySingleton.getGold()<<" "<<mySingleton.getExperience()<<" "<<mySingleton.getLevel()<<" "<<mySingleton.getSocial()<<" "<<mySingleton.getStrength()<<" "<<mySingleton.getIntelligence()<<" "<<mySingleton.getStress()<<"\n";
    
    //draw backarrow
    auto backButton = cocos2d::ui::Button::create("images/backarrow.png","", "");
    backButton->setPosition(Vec2(visibleSize.width*0.05 + origin.x, visibleSize.height * 0.95 + origin.y));
    backButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(backButton);
    
    //add selection buttons
    auto textField = cocos2d::ui::TextField::create("New game name","Arial",18);
    textField->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.55 + origin.y));
    textField->setMaxLength(10);
    textField->setMaxLengthEnabled(true);
    textField->addEventListener(CC_CALLBACK_2(MultiplayerMenu::textFieldUsed, this));
    
    this->addChild(textField);
    
    auto createButton = cocos2d::ui::Button::create("images/CreateButtonBefore.png","images/CreateButtonAfter.png", "");
    createButton->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height * 0.40 + origin.y));
    createButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->pushScene(MultiplayerGame::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(createButton);
    
    auto joinButton = cocos2d::ui::Button::create("images/JoinButtonBefore.png","images/JoinButtonAfter.png", "");
    joinButton->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height * 0.25 + origin.y));
    joinButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->pushScene(MultiplayerGame::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(joinButton);
    
    //check if have team
//    getBattleStats();
    
    return true;
}

//textfield
void MultiplayerMenu::textFieldUsed(Ref * pSender, cocos2d::ui::TextField::EventType type)
{
    cocos2d::ui::TextField * textfield = dynamic_cast<cocos2d::ui::TextField *>(pSender);
    switch (type) {
        case cocos2d::ui::TextField::EventType::ATTACH_WITH_IME:
            cout<<"\ntextfield:attached";
            break;
        case cocos2d::ui::TextField::EventType::DELETE_BACKWARD:
            cout<<"\ntextfield:delete text";
            roomName = textfield->getString();
            break;
        case cocos2d::ui::TextField::EventType::DETACH_WITH_IME:
            cout<<"\ntextfield:detached";
            break;
        case cocos2d::ui::TextField::EventType::INSERT_TEXT:
            cout<<"\ntextfield:insert text";
            roomName = textfield->getString();
            break;
        default:
            break;
    }
}
