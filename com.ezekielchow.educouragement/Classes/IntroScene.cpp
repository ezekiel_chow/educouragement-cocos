//
//  IntroScene.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 17/03/2017.
//
//

#include "MainMenu.hpp"
#include "IntroScene.hpp"
USING_NS_CC;

Scene* IntroScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = IntroScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool IntroScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite * space = Sprite::create("images/Space.jpg");
    Size wSize = Size(Director::getInstance()->getWinSizeInPixels().width, Director::getInstance()->getWinSizeInPixels().height);
    Size scrSize= Size(1136, 640);
    space->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    space->setScaleX(wSize.width/scrSize.width);
    space->setScaleY(wSize.height/scrSize.height);
    this->addChild(space);
    
    cocos2d::ui::Text *story = ui::Text::create("In Earth's future, a global crop blight and second Dust Bowl are slowly rendering the planet uninhabitable. Professor Brand (Michael Caine), a brilliant NASA physicist, is working on plans to save mankind by transporting Earth's population to a new home via a wormhole. But first, Brand must send former a team of researchers through the wormhole and across the galaxy to find out which of three planets could be mankind's new home. YOU have been chosen.", "fonts/arial.ttf", 10);
    story->setTextAreaSize(Size(visibleSize.width*0.8, visibleSize.height*0.6));
    story->setTextHorizontalAlignment(TextHAlignment::CENTER);
    story->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*-0.3 + origin.y));
    this->addChild(story);
    
    //
    auto skipButton = cocos2d::ui::Button::create("images/SkipBefore.png", "images/SkipAfter.png", "");
    skipButton->setPosition(Vec2(visibleSize.width*0.85 + origin.x, (visibleSize.height * 0.05) + origin.y));
    skipButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(skipButton);
    
    auto action = cocos2d::MoveTo::create(15, Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    story->runAction(action);
    
    return true;
}
