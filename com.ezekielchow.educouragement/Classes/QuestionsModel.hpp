//
//  QuestionsModel.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 13/03/2017.
//
//

#ifndef QuestionsModel_hpp
#define QuestionsModel_hpp

#include <stdio.h>
class QuestionSet
{
public:
    void init(std::string question, std::string answerone, std::string answertwo, std::string answerthree, std::string answerfour, int correctanswer);
    
    std::string getQuestion();
    std::string getAnswerOne();
    std::string getAnswerTwo();
    std::string getAnswerThree();
    std::string getAnswerFour();
    int getCorrectAnswer();
    
    void setQuestion(std::string question);
    void setAnswerOne(std::string answerone);
    void setAnswerTwo(std::string answertwo);
    void setAnswerThree(std::string answerthree);
    void setAnswerFour(std::string answerfour);
    void setCorrectAnswer(int correctanswer);
private:
    std::string questionR, answeroneR, answertwoR, answerthreeR, answerfourR;
    int correctanswerR;
};
#endif /* QuestionsModel_hpp */
