//
//  EndGame.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 20/03/2017.
//
//

#ifndef EndGame_hpp
#define EndGame_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PlayerDataSingleton.hpp"
#include "MainMenu.hpp"
#include "PluginFacebook/PluginFacebook.h"
#include <iostream>

class EndGame : public cocos2d::Layer, sdkbox::FacebookListener
{
private:
    PlayerDataSingleton mySingleton;
    cocos2d::ui::Button *continueButton;
    
    virtual void drawNodes();
    virtual void updateQuestData();
    virtual void updateSuccess(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    virtual void drawDialog(std::string message);
    
    virtual void onLogin(bool isLogin, const std::string& msg);
    virtual void onSharedSuccess(const std::string& message);
    virtual void onSharedFailed(const std::string& message);
    virtual void onSharedCancel();
    virtual void onAPI(const std::string& key, const std::string& jsonData);
    virtual void onPermission(bool isLogin, const std::string& msg);
    virtual void onFetchFriends(bool ok, const std::string& msg);
    virtual void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends );
    virtual void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg );
    virtual void onInviteFriendsResult( bool result, const std::string& msg );
    virtual void onGetUserInfo( const sdkbox::FBGraphUser& userInfo);
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(EndGame);
};

#endif /* EndGame_hpp */
