//
//  FacebookLogin.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 19/03/2017.
//
//

#include "FacebookLogin.hpp"
#include <iostream>
USING_NS_CC;

Scene* FacebookLogin::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = FacebookLogin::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool FacebookLogin::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    sdkbox::PluginFacebook::setListener(this);
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    //check if already login
    if (sdkbox::PluginFacebook::isLoggedIn())
    {
        std::cout<<"\n\n\nshotuing"<<sdkbox::PluginFacebook::getUserID();
        Director::getInstance()->pushScene(IntroScene::createScene());
    }
    else
    {
        //add facebook login
        auto loginButton = cocos2d::ui::Button::create("images/facebook_button.png", "images/facebook_button.png", "");
        
        loginButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
            switch (type)
            {
                case ui::Widget::TouchEventType::BEGAN:
                break;
                case ui::Widget::TouchEventType::ENDED:
                sdkbox::PluginFacebook::login();
                break;
                default:
                break;
            }
        });
        
        loginButton->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
        loginButton->setTag(10);
        this->addChild(loginButton);
    }
    
    return true;
}

//Facebook methods
void FacebookLogin::onLogin(bool isLogin, const std::string &msg)
{
    if (isLogin==true) {
        std::cout<<"login succeed"+msg;
        Director::getInstance()->pushScene(IntroScene::createScene());
    }
    else{
        std::cout<<"login fail";
    }
}

void FacebookLogin::onPermission(bool isLogin, const std::string &msg)
{}

void FacebookLogin::onAPI(const std::string &tag, const std::string &jsonData)
{}

void FacebookLogin::onSharedSuccess(const std::string &message)
{}

void FacebookLogin::onSharedCancel()
{}

void FacebookLogin::onSharedFailed(const std::string &message)
{}

void FacebookLogin::onFetchFriends(bool ok, const std::string &msg)
{}

void FacebookLogin::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo &friends)
{}

void FacebookLogin::onInviteFriendsWithInviteIdsResult(bool result, const std::string &msg)
{}

void FacebookLogin::onInviteFriendsResult(bool result, const std::string &msg)
{}

void FacebookLogin::onGetUserInfo(const sdkbox::FBGraphUser &userInfo)
{}
