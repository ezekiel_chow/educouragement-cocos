//
//  Game.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 12/03/2017.
//
//

#include "Game.hpp"
#include <iostream>
#include "external/rapidjson/reader.h"
#include "external/rapidjson/writer.h"
#include "external/rapidjson/document.h"
#include "external/rapidjson/stringbuffer.h"
#include "MultiplayerGame.hpp"

#define COCOS2D_DEBUG 1

USING_NS_CC;
using namespace std;
using namespace rapidjson;

Scene* Game::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Game::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Game::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(135,206,250,255)) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    cout<<"screensize:"<<Director::getInstance()->getWinSize().width<<"o:"<<Director::getInstance()->getWinSize().height;
    mySingleton = PlayerDataSingleton();
    topicSelected = "";
    selectedLevel = 1;
    
    drawMainGame();
    
    return true;
}

void Game::drawMainGame()
{
    this->removeAllChildren();
    this->cleanup();
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    //background
    Sprite * space = Sprite::create("images/Space.jpg");
    Size wSize = Size(Director::getInstance()->getWinSizeInPixels().width, Director::getInstance()->getWinSizeInPixels().height);
    Size scrSize= Size(1136, 640);
    space->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    space->setScaleX(wSize.width/scrSize.width);
    space->setScaleY(wSize.height/scrSize.height);
    
    this->addChild(space);
    
    
    Sprite *lifeBarFrame = Sprite::create("images/BarFrame.png");
    lifeBarFrame->setPosition(visibleSize.width/2 + origin.x, visibleSize.height*0.9 + origin.y);
    
    Sprite *lifeSprite = Sprite::create("images/LifeBar.png");
    lifeBar = ProgressTimer::create(lifeSprite);
    lifeBar->setPosition(visibleSize.width/2 + origin.x, visibleSize.height*0.9 + origin.y);
    lifeBar->setType(ProgressTimer::Type::BAR);
    lifeBar->setBarChangeRate(Vec2(1, 0));
    lifeBar->setMidpoint(Vec2(0.0f, 0.1f));
    lifeBar->setPercentage(100);
    this->addChild(lifeBar);
    this->addChild(lifeBarFrame);
    
    Sprite *skillBarFrame = Sprite::create("images/BarFrame.png");
    skillBarFrame->setPosition(visibleSize.width/2 + origin.x, visibleSize.height*0.85 + origin.y);
    
    Sprite *skillSprite = Sprite::create("images/SkillBar.png");
    skillBar = ProgressTimer::create(skillSprite);
    skillBar->setPosition(visibleSize.width/2 + origin.x, visibleSize.height*0.85 + origin.y);
    skillBar->setType(ProgressTimer::Type::BAR);
    skillBar->setBarChangeRate(Vec2(1, 0));
    skillBar->setMidpoint(Vec2(0.0f, 0.1f));
    skillBar->setPercentage(65);
    this->addChild(skillBar);
    this->addChild(skillBarFrame);
    
    question = ui::Text::create("Lorem ipsum dolor sit amet, consectetur adipiscing volutpat.", "fonts/arial.ttf", 16);
    question->setTextAreaSize(Size(visibleSize.width*0.9, visibleSize.height*0.30));
    question->setTextHorizontalAlignment(TextHAlignment::CENTER);
    question->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.6 + origin.y));
    this->addChild(question);
    
    answer1 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png","images/AnswerButtonAfter.png", "");
    answer1->setTitleColor(Color3B::BLACK);
    answer1->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.44 + origin.y));
    answer1->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                cout<<"\n\nparticle suppose to appear";
                if(checkAnswer(1) == true)
                {
                    this->animateCorrect(answer1->getPosition(), skillBar->getPercentage());
                }
                else //wrong
                {
                    this->animateWrong(answer1->getPosition());
                }
                break;
            default:
                break;
            
        }
    });
    this->addChild(answer1);
    
    answer2 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png","images/AnswerButtonAfter.png", "");
    answer2->setTitleColor(Color3B::BLACK);
    answer2->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.32 + origin.y));
    answer2->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                if(checkAnswer(2) == true)
                {
                    this->animateCorrect(answer2->getPosition(), skillBar->getPercentage());
                }
                else //wrong
                {
                    this->animateWrong(answer2->getPosition());
                }
                break;
            default:
                break;
        }
    });
    
    this->addChild(answer2);
    answer3 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png","images/AnswerButtonAfter.png", "");
    answer3->setTitleColor(Color3B::BLACK);
    answer3->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.20 + origin.y));
    answer3->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                if(checkAnswer(3) == true)
                {
                    this->animateCorrect(answer3->getPosition(), skillBar->getPercentage());
                }
                else //wrong
                {
                    this->animateWrong(answer3->getPosition());
                }
                break;
            default:
                break;
        }
    });
    this->addChild(answer3);
    
    answer4 = cocos2d::ui::Button::create("images/AnswerButtonBefore.png","images/AnswerButtonAfter.png", "");
    answer4->setTitleColor(Color3B::BLACK);
    answer4->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.08 + origin.y));
    answer4->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                if(checkAnswer(4) == true)
                {
                    this->animateCorrect(answer4->getPosition(), skillBar->getPercentage());
                }
                else //wrong
                {
                    this->animateWrong(answer4->getPosition());
                }
                break;
            default:
                break;
        }
    });
    this->addChild(answer4);
    
    getGameData(); //get questions
    startGame(); //start game
}

void Game::animateCorrect(Vec2 buttonPosition, float barPercentage)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    ParticleSystemQuad *emitter = new ParticleSystemQuad();
    emitter = ParticleGalaxy::create();
    emitter->setEmitterMode(ParticleSystemQuad::Mode::GRAVITY);
    emitter->cocos2d::Node::setScale(0.5, 0.5);
    emitter->setTotalParticles(500);
    emitter->setGravity(Vec2(0, -180));
    
    float randomNum = RandomHelper::random_int(10, 90);
    emitter->setPosition(Vec2(visibleSize.width*(randomNum/100) + origin.x, buttonPosition.y));
    this->addChild(emitter);
    
    auto action = cocos2d::MoveTo::create(1, skillBar->getPosition());
    auto removeSelf = RemoveSelf::create();
    auto sequence = Sequence::create(action, removeSelf, NULL);
    emitter->runAction(sequence);
}

void Game::animateWrong(Vec2 buttonPosition)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    ParticleSystemQuad *smoke = new ParticleSystemQuad();
    smoke = ParticleSmoke::create();
    smoke->setEmitterMode(ParticleSystemQuad::Mode::GRAVITY);
    smoke->cocos2d::Node::setScale(0.5, 0.5);
    smoke->setTotalParticles(30);
    smoke->setOpacity(0.5*255);
    smoke->setGravity(Vec2(0, 0));
    float randomNum = RandomHelper::random_int(10, 90);
    smoke->setPosition(Vec2(visibleSize.width*(randomNum/100) + origin.x, buttonPosition.y));
    
    this->addChild(smoke);
    
    ParticleSystemQuad *fire = new ParticleSystemQuad();
    fire = ParticleFire::create();
    fire->setEmitterMode(ParticleSystemQuad::Mode::GRAVITY);
    fire->cocos2d::Node::setScale(0.5, 0.5);
    fire->setTotalParticles(30);
    fire->setOpacity(0.3*255);
    fire->setGravity(Vec2(0, 0));
    fire->setPosition(Vec2(visibleSize.width*(randomNum/100) + origin.x, buttonPosition.y));
    
    this->addChild(fire);
    
    auto action = cocos2d::FadeTo::create(2, 0);
    auto removeSelf = RemoveSelf::create();
    auto sequence = Sequence::create(action, removeSelf, NULL);
    fire->runAction(sequence);
    
    auto action2 = cocos2d::FadeTo::create(2.5, 0);
    auto sequence2 = Sequence::create(action2, removeSelf, NULL);
    smoke->runAction(sequence2);
    
}

void Game::getGameData()
{
    std::string questionType;
    if (mySingleton.getQuestSelected() == 1) {
        questionType = "HTML";
    }
    else if(mySingleton.getQuestSelected() == 2)
    {
        questionType = "CSS";
    }
    else
    {
        cout<<"please fix";
        questionType = "HTML";
    }
    
    //gettting questions
    cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
    request->setUrl("https://educouragement-rest.herokuapp.com/api/questions/" + questionType);
    request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    request->setResponseCallback( CC_CALLBACK_2(Game::getQuestions, this) );
    request->setTag("GET HtmlQuestions");
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
    
    //life and skill
    life = mySingleton.getStrength();
    skill = mySingleton.getIntelligence();
    skillPercentageForOneSecond = 100/skill;
    lifeConvertedToPercentage = 100/life;
}

void Game::getQuestions(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        log("onHttpRequestCompleted - No Response");
        return;
    }
    
    if (!response->isSucceed())
    {
        log("onHttpRequestCompleted - Response failed");
        log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    char * concatenated = (char *) malloc(buffer->size() + 1);
    std::string s2(buffer->begin(), buffer->end());
    strcpy(concatenated, s2.c_str());
    
    Document document;
    document.Parse(concatenated);
    
    // 3. Stringify the DOM
    StringBuffer stringBuffer;
    Writer<rapidjson::StringBuffer> writer(stringBuffer);
    document.Accept(writer);
    
    assert(document.IsObject());
    assert(document["data"]["rowCount"].IsInt());
    
    if (document["data"]["rowCount"].GetInt() > 0) { //got questions
        assert(document["data"]["rows"].IsArray());
        
        std::string question, answerone, answertwo, answerthree, answerfour;
        int questionmode, correctanswer;
        
        static const char* kTypeNames[] = { "Null", "False", "True", "Object", "Array", "String", "Number" };
//        for (rapidjson::Value::ConstValueIterator itr = document["data"]["rows"].Begin(); itr != document["data"]["rows"].End(); ++itr)
        
        
        //iterate array
        for (rapidjson::Value::ConstValueIterator arrayitr = document["data"]["rows"].Begin(); arrayitr != document["data"]["rows"].End(); ++arrayitr)
        {
            QuestionSet myQuestion;
            std::string question, answerone, answertwo, answerthree, answerfour;
            int correctanswer;
            //iterate one question details
            for (rapidjson::Value::ConstMemberIterator objitr = arrayitr->GetObject().MemberBegin();
                 objitr != arrayitr->GetObject().MemberEnd(); ++objitr)
            {
                
                if (strcmp(objitr->name.GetString(), "question") == 0) {
                    question = objitr->value.GetString();
                }
                else if (strcmp(objitr->name.GetString(), "answerone") == 0) {
                    if (strcmp(objitr->value.GetString(), "none") == 0) {
                        answerone = "100% wrong";
                    }
                    else
                    {
                        answerone = objitr->value.GetString();
                    }
                }
                else if (strcmp(objitr->name.GetString(), "answertwo") == 0) {
                    if (strcmp(objitr->value.GetString(), "none") == 0) {
                        answertwo = "100% wrong";
                    }
                    else
                    {
                        answertwo = objitr->value.GetString();
                    }
                }
                else if (strcmp(objitr->name.GetString(), "answerthree") == 0) {
                    if (strcmp(objitr->value.GetString(), "none") == 0) {
                        answerthree = "100% wrong";
                    }
                    else
                    {
                        answerthree = objitr->value.GetString();
                    }
                }
                else if (strcmp(objitr->name.GetString(), "answerfour") == 0) {
                    if (strcmp(objitr->value.GetString(), "none") == 0) {
                        answerfour = "100% wrong";
                    }
                    else
                    {
                        answerfour = objitr->value.GetString();
                    }
                }
                else if (strcmp(objitr->name.GetString(), "correctanswer") == 0) {
                    correctanswer = objitr->value.GetInt();
                }
                else
                {//uncomment for checking
//                    cout<<"\nThrow away member"<<objitr->name.GetString();
                }
            }
            myQuestion.init(question, answerone, answertwo, answerthree, answerfour, correctanswer);
            allQuestions.push_back(myQuestion);
        }
//        for (std::vector<QuestionSet>::iterator it = allQuestions.begin() ; it != allQuestions.end(); ++it) {
//            QuestionSet aSet = *it;
//            cout<<"\nyesh?"<<aSet.getQuestion()<<" ans:"<<aSet.getCorrectAnswer()<<" emptyans: "<<aSet.getAnswerThree();
//        }
    }
    questionCounter = 0;
    loadQuestions(questionCounter);
}

void Game::loadQuestions(int questionNum)
{
    QuestionSet oneSet = allQuestions[questionNum];
    question->setString(oneSet.getQuestion() + std::to_string(oneSet.getCorrectAnswer()) );
    answer1->setTitleText(oneSet.getAnswerOne());
    answer2->setTitleText(oneSet.getAnswerTwo());
    answer3->setTitleText(oneSet.getAnswerThree());
    answer4->setTitleText(oneSet.getAnswerFour());
    currentCorrectAnswer = oneSet.getCorrectAnswer();
}

void Game::startGame()
{
    correctAnsCount = 0;
    wrongAnsCount = 0;
    lifeBar->setPercentage(100);
    skillBar->setPercentage(100);
    this->schedule(schedule_selector(Game::updateGame), 1);
}

bool Game::skillBarEmpty()
{
    if (skillBar->getPercentage() == 0 || skillBar->getPercentage() < 0) {
        return true;
    }
    else
    {
        return false;
    }
}

bool Game::lifeBarEmpty()
{
    if (lifeBar->getPercentage() == 0 || lifeBar->getPercentage() < 0) {
        return true;
    }
    else
    {
        return false;
    }
}

void Game::updateGame(float delta) //every second
{
    //calculate skill
    skillBar->setPercentage(skillBar->getPercentage() - skillPercentageForOneSecond);
    
    if (Game::skillBarEmpty()) {
        cout<<"\nHERE: "<<lifeBar->getPercentage()<<" "<<lifeConvertedToPercentage<<" "<<skill;
        lifeBar->setPercentage(lifeBar->getPercentage() - lifeConvertedToPercentage);
        
        if (!Game::lifeBarEmpty()) {
            //respawn
            float randomNum = RandomHelper::random_int(40, 80);
            skillBar->setPercentage(randomNum);
        }
        else
        {
            //show ending screen
            mySingleton.setMissonSucceed(false);
            this->unschedule(schedule_selector(Game::updateGame));
            loadEndingScreen();
        }
    }
}

bool Game::checkAnswer(int playerAnswer)
{
    if (playerAnswer == currentCorrectAnswer) {
        correctAnsCount += 1;
        cout<<"\n\ncorrect!: "<<playerAnswer<<" "<<currentCorrectAnswer;
        skillBar->setPercentage(skillBar->getPercentage() + (skillPercentageForOneSecond*3));
        
        //check if there is more ans
        questionCounter++;
        if (questionCounter >= allQuestions.size() )
        {
            //show ending screen
            mySingleton.setMissonSucceed(true);
            this->unschedule(schedule_selector(Game::updateGame));
            loadEndingScreen();
        }
        else
        {
            loadQuestions(questionCounter);
        }
        return true;
    }
    else
    { //wrong answer
        wrongAnsCount += 1;
        cout<<"\n\wrong!: "<<playerAnswer<<" "<<currentCorrectAnswer;
        if (skillBar->getPercentage() > 0)
        {//still have mana
            skillBar->setPercentage(skillBar->getPercentage() - (skillPercentageForOneSecond * 2));
            
            questionCounter++;
            if (questionCounter >= allQuestions.size() )
            {
                //show ending screen
                mySingleton.setMissonSucceed(true);
                this->unschedule(schedule_selector(Game::updateGame));
                loadEndingScreen();
            }
            else
            {
                loadQuestions(questionCounter);
            }
        }
        else if(Game::skillBarEmpty())
        {//no more mana
            lifeBar->setPercentage(lifeBar->getPercentage() - lifeConvertedToPercentage);
            if (Game::lifeBarEmpty()) { //no more life too
                //show ending screen
                mySingleton.setMissonSucceed(false);
                this->unschedule(schedule_selector(Game::updateGame));
                loadEndingScreen();
            }else
            {//respawn
                float randomNum = RandomHelper::random_int(40, 80);
                skillBar->setPercentage(randomNum);
            }
        }
        return false;
    }
}

void Game::loadEndingScreen()
{
    mySingleton.setWrongTotal(wrongAnsCount);
    mySingleton.setCorrectTotal(correctAnsCount);
    Director::getInstance()->replaceScene(EndGame::createScene());
}
