//
//  MainMenu.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 08/03/2017.
//
//

#include "Settings.hpp"
#include "MainMenu.hpp"
#include "ui/CocosGUI.h"
#include <iostream>
#include "external/rapidjson/reader.h"
#include "external/rapidjson/writer.h"
#include "external/rapidjson/document.h"
#include "external/rapidjson/stringbuffer.h"
#include "MultiplayerMenu.hpp"
#include "Game.hpp"
#include "QuestSelect.hpp"


#define COCOS2D_DEBUG 1

USING_NS_CC;
using namespace std;
using namespace rapidjson;

Scene* MainMenu::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MainMenu::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(135,206,250,255)) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    mySingleton = PlayerDataSingleton();
    
    drawBackground();
    
    sdkbox::PluginFacebook::setListener(this);
    cout<<"\nFBID: "<<sdkbox::PluginFacebook::getUserID();
    
    //Install menu
    drawMenu();
    
    //play button
    auto playButton = cocos2d::ui::Button::create("images/PlayButtonBefore.png", "images/PlayButtonAfter.png", "");
    playButton->setPosition(Vec2(visibleSize.width/2 + origin.x, (visibleSize.height * 0.20) + origin.y));
    playButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(QuestSelect::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(playButton, 10);
    
    //attributes
    cout<<"\nFacebookID: "<<sdkbox::PluginFacebook::getUserID();
    std::string baseUrl = "https://educouragement-rest.herokuapp.com/api/player/";
    std::string completeURL = baseUrl + sdkbox::PluginFacebook::getUserID();
    
    cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
    request->setUrl(completeURL);
    request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    request->setResponseCallback( CC_CALLBACK_2(MainMenu::checkIfNewPlayer, this) );
    request->setTag("GET NewPlayerCheck");
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
    
    //Facebook
    checkFacebookPermissions();
    
    return true;
}

void MainMenu::drawBackground()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Image * spaceImage = new  Image ();
    spaceImage->initWithImageFile("images/Space.jpg");
    Texture2D * spaceTexture = new Texture2D();
    spaceTexture-> initWithImage (spaceImage);
    Sprite * space = Sprite::createWithTexture(spaceTexture);
    Size wSize = Size(Director::getInstance()->getWinSizeInPixels().width, Director::getInstance()->getWinSizeInPixels().height);
    Size scrSize= Size(1136, 640);
    space->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    space->setScaleX(wSize.width/scrSize.width);
    space->setScaleY(wSize.height/scrSize.height);
    this->addChild(space);
}

void MainMenu::drawMenu()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto leaderboardsMenu = cocos2d::ui::Button::create("images/trophy.png", "images/trophy.png", "");
    leaderboardsMenu->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.05 + origin.y));
    leaderboardsMenu->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(Leaderboards::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(leaderboardsMenu, 10);
    
    auto shopMenu = cocos2d::ui::Button::create("images/shopping_cart.png", "images/shopping_cart.png", "");
    shopMenu->setPosition(Vec2(visibleSize.width*0.35 + origin.x, visibleSize.height*0.05 + origin.y));
    shopMenu->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(Shop::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(shopMenu, 10);
    
    auto multiplayerMenu = cocos2d::ui::Button::create("images/monsterx75.png", "images/monsterx75.png", "");
    multiplayerMenu->setPosition(Vec2(visibleSize.width*0.65 + origin.x, visibleSize.height*0.05 + origin.y));
    multiplayerMenu->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MultiplayerMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(multiplayerMenu, 10);
    
    auto settingsMenu = cocos2d::ui::Button::create("images/settings.png", "images/settings.png", "");
    settingsMenu->setPosition(Vec2(visibleSize.width*0.9 + origin.x, visibleSize.height*0.05 + origin.y));
    settingsMenu->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(Settings::createScene());
                break;
            default:
                break;
        }
    });
    this->addChild(settingsMenu, 10);
}

void MainMenu::navigateTo(cocos2d::Ref *sender, cocos2d::Scene *scene)
{
    Director::getInstance()->replaceScene(scene);
}

void MainMenu::checkIfNewPlayer(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        log("onHttpRequestCompleted - No Response");
        return;
    }
    
    if (!response->isSucceed())
    {
        log("onHttpRequestCompleted - Response failed");
        log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    char * concatenated = (char *) malloc(buffer->size() + 1);
    std::string s2(buffer->begin(), buffer->end());
    strcpy(concatenated, s2.c_str());
    
    Document document;
    document.Parse(concatenated);
    
    // 3. Stringify the DOM
    StringBuffer stringBuffer;
    Writer<rapidjson::StringBuffer> writer(stringBuffer);
    document.Accept(writer);
    
    assert(document.IsObject());
    assert(document["data"]["rowCount"].IsInt());
    
    if (document["data"]["rowCount"].GetInt() > 0) { //old player
        assert(document["data"]["rows"][0].IsObject());
        
        document["data"]["rows"][0].FindMember("playerid");
        
        std::string team;
        int strength, intelligence, stress, social, level, experience, gold;
        vector<int> items;
        static const char* kTypeNames[] = { "Null", "False", "True", "Object", "Array", "String", "Number" };
        for (rapidjson::Value::ConstMemberIterator itr = document["data"]["rows"][0].MemberBegin(); itr != document["data"]["rows"][0].MemberEnd(); ++itr)
            printf("Type of member %s is %s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
        
        for (rapidjson::Value::ConstMemberIterator itr = document["data"]["rows"][0].MemberBegin();
             itr != document["data"]["rows"][0].MemberEnd(); ++itr)
        {
            if (strcmp(itr->name.GetString(), "team") == 0) {
                team = itr->value.GetString();
                mySingleton.setTeam(itr->value.GetString());
            }
            else if (strcmp(itr->name.GetString(), "strength") == 0) {
                strength = itr->value.GetInt();
                mySingleton.setStrength(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "intelligence") == 0) {
                intelligence = itr->value.GetInt();
                mySingleton.setIntelligence(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "stress") == 0) {
                stress = itr->value.GetInt();
                mySingleton.setStress(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "social") == 0) {
                social = itr->value.GetInt();
                mySingleton.setSocial(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "level") == 0) {
                level = itr->value.GetInt();
                mySingleton.setLevel(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "experience") == 0) {
                experience = itr->value.GetInt();
                mySingleton.setExperience(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "gold") == 0) {
                gold = itr->value.GetInt();
                mySingleton.setGold(itr->value.GetInt());
            }
            else if (strcmp(itr->name.GetString(), "items") == 0) {
                for (rapidjson::Value::ConstValueIterator itrArray = itr->value.GetArray().Begin(); itrArray != itr->value.GetArray().End(); ++itrArray)
                {
                    items.push_back(itrArray->GetInt());
                }
            }
            else
            {
                cout<<"\nThrow away member"<<itr->name.GetString();
            }
        }
        mySingleton.setItems(items);
        drawAttributes(strength, intelligence, stress, social, level, experience, team, gold);
        
    }
    else //new player. install player
    {
        std::string baseUrl = "https://educouragement-rest.herokuapp.com/api/player";
        
        cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        request->setUrl(baseUrl);
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback( CC_CALLBACK_2(MainMenu::createAttributes, this) );
        std::string requestData= "playerid=" + sdkbox::PluginFacebook::getUserID() + "&strength=2&intelligence=2&stress=5&social=5&level=1&experience=0&team=none&gold=0";
        const char* postData = requestData.c_str();
        request->setRequestData(postData, strlen(postData));
        request->setTag("POST InstallPlayer");
        cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void MainMenu::createAttributes(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        log("onHttpRequestCompleted - No Response");
        return;
    }
    
    if (!response->isSucceed())
    {
        log("onHttpRequestCompleted - Response failed");
        log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    char * concatenated = (char *) malloc(buffer->size() + 1);
    std::string s2(buffer->begin(), buffer->end());
    strcpy(concatenated, s2.c_str());
    
    Document document;
    document.Parse(concatenated);
    
    // 3. Stringify the DOM
    StringBuffer stringBuffer;
    Writer<rapidjson::StringBuffer> writer(stringBuffer);
    document.Accept(writer);
    
    assert(document.IsObject());
    assert(document["status"].IsString());
    
    cout<<"create user:"<<concatenated<<"<";
    drawAttributes(2, 2, 5, 5, 1, 0, "none", 0);
}

void MainMenu::drawAttributes(int strength, int intelligence, int stress, int social, int level, int experience, std::string team, int gold)
{
    cout<<"\ndrawing attributes for: strength:"<<strength<<" intelligence:"<<intelligence<<" stress:"<<stress<<" social:"<<social<<" level:"<<level<<" experience:"<<experience<<" team:"<<team<<" gold:"<<gold;
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    //touch listener
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](Touch* touch, Event* event){
        // event->getCurrentTarget() returns the *listener's* sceneGraphPriority node.
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
            if (target->getTag() == 30) {
                this->drawDialog("Amount of oxygen tanks");
            }
            else if(target->getTag() == 31)
            {
                this->drawDialog("Size of an oxygen tank");
            }
            else if (target->getTag() == 32)
            {
                this->drawDialog("62 exp to next level");
            }
            
            return true;
        }
        
        return false;
    };
    listener->onTouchEnded = [&](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        log("sprite onTouchesEnded.. ");
        this->removeChildByTag(77);
        //Reset zOrder and the display sequence will change
        
    };
    
    //strength icon
    Sprite * strengthSprite = Sprite::create("images/OxygenTank.png");
    strengthSprite->setPosition(Vec2(visibleSize.width*0.3 + origin.x, visibleSize.height*0.50 + origin.y));
    strengthSprite->setTag(30);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, strengthSprite);
    strengthSprite->setEventDispatcher(Director::getInstance()->getEventDispatcher());
    this->addChild(strengthSprite);
    
    //intel icon
    Sprite * intelligenceSprite = Sprite::create("images/TankSize.png");
    intelligenceSprite->setPosition(Vec2(visibleSize.width*0.3 + origin.x, visibleSize.height*0.40 + origin.y));
    intelligenceSprite->setTag(31);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener->clone(), intelligenceSprite);
    intelligenceSprite->setEventDispatcher(Director::getInstance()->getEventDispatcher());
    this->addChild(intelligenceSprite);
    
    //base n extra strength
    auto strengthLabel = Label::createWithTTF(std::to_string(strength) , "fonts/arial.ttf", 16);
    strengthLabel->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.50 + origin.y));
    this->addChild(strengthLabel);
    
    //base n extra intel
    auto intelligenceLabel = Label::createWithTTF(std::to_string(intelligence) , "fonts/arial.ttf", 16);
    intelligenceLabel->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.40 + origin.y));
    this->addChild(intelligenceLabel);
    
    //experience bar
    Sprite *expBarFrame = Sprite::create("images/ExpFrame.png");
    expBarFrame->setPosition(visibleSize.width/2 + origin.x, visibleSize.height*0.60 + origin.y);
    
    Sprite *expSprite = Sprite::create("images/ExpBar.png");
    expBar = ProgressTimer::create(expSprite);
    expBar->setPosition(visibleSize.width/2 + origin.x, visibleSize.height*0.60 + origin.y);
    expBar->setType(ProgressTimer::Type::BAR);
    expBar->setBarChangeRate(Vec2(1, 0));
    expBar->setMidpoint(Vec2(0.0f, 0.1f));
    expBar->setPercentage(38);
    expBar->setTag(32);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener->clone(), expBar);
    expBar->setEventDispatcher(Director::getInstance()->getEventDispatcher());
    this->addChild(expBar);
    this->addChild(expBarFrame);
    
    auto currentPlayerLevel = Label::createWithTTF("Level " + std::to_string(mySingleton.getLevel()), "fonts/arial.ttf", 9);
    currentPlayerLevel->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.60 + origin.y));
    this->addChild(currentPlayerLevel, 20);
}

void MainMenu::drawDialog(std::string message)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto infoLabel = Label::createWithTTF(message , "fonts/arial.ttf", 12);
    infoLabel->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.95 + origin.y));
    infoLabel->setTag(77);
    this->addChild(infoLabel, 10);
}

void MainMenu::checkFacebookPermissions()
{
    vector<std::string> permissions;
    permissions = sdkbox::PluginFacebook::getPermissionList();
    
    bool emailCheck = false;
    bool public_profileCheck = false;
    for(vector<string>::iterator it = permissions.begin(); it != permissions.end(); ++it) {
        string currentIt = *it;
        if (currentIt == "email")
        {
            emailCheck = true;
        }
        else if (currentIt == "public_profile")
        {
            public_profileCheck = true;
        }
    }
    
    vector<string> permissionToRequest;
    if (emailCheck == false || public_profileCheck == false)
    {
        if (emailCheck == false)
        {
            permissionToRequest.push_back(sdkbox::FB_PERM_READ_EMAIL);
            emailCheck = true;
        }
        if (public_profileCheck == false)
        {
            permissionToRequest.push_back(sdkbox::FB_PERM_READ_PUBLIC_PROFILE);
            public_profileCheck = true;
        }
        sdkbox::PluginFacebook::requestReadPermissions(permissionToRequest);
    }
    
    if (emailCheck == true && public_profileCheck == true) {
        sdkbox::FBAPIParam params;
        params.insert(pair<string, string>("redirect", "0"));
        params.insert(pair<string, string>("width", "250"));
        params.insert(pair<string, string>("height", "250"));
        sdkbox::PluginFacebook::api("/me/picture", "GET", params, "profilePhoto");
    }
}

//Facebook methods
void MainMenu::onLogin(bool isLogin, const std::string &msg)
{ cout<<"\nFBAPICALL(onLogin)"; }

void MainMenu::onPermission(bool isLogin, const std::string &msg)
{ cout<<"\nFBAPICALL(onPermission)"; }

void MainMenu::onAPI(const std::string &tag, const std::string &jsonData)
{ cout<<"\nFBAPICALL(onAPI)";
    
    if (tag == "profilePhoto")
    {
        Document document;
        document.Parse(jsonData.c_str());

        // 3. Stringify the DOM
        StringBuffer buffer;
        Writer<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);
        
        CCLOG("json %s", buffer.GetString());
        assert(document.IsObject());
        assert(document["data"]["url"].IsString());
        
        cocos2d::network::HttpRequest* request = new (std::nothrow) cocos2d::network::HttpRequest();
        request->setUrl(document["data"]["url"].GetString());
        request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
        request->setResponseCallback(CC_CALLBACK_2(MainMenu::onRequestImgCompleted, this));
        request->setTag("GET FBImage");
        cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
        
        // Iterating object members (uncomment to check type..make life easier)
//        static const char* kTypeNames[] = { "Null", "False", "True", "Object", "Array", "String", "Number" };
//        for (Document::ValueType::ConstMemberIterator itr = document.MemberBegin(); itr != document.MemberEnd(); ++itr)
//            printf("Type of member %s is %s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
    }
}

void MainMenu::onRequestImgCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        log("onHttpRequestCompleted - No Response");
        return;
    }
    
    if (!response->isSucceed())
    {
        log("onHttpRequestCompleted - Response failed");
        log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    std::vector<char> *buffer = response->getResponseData();
    
    Image * image = new  Image ();
    image-> initWithImageData ( reinterpret_cast<const unsigned char*>(&(buffer->front())), buffer->size());
    Texture2D * texture = new Texture2D();
    texture-> initWithImage (image);
    Sprite * profSprite = Sprite :: createWithTexture (texture);
    
    auto stencil = DrawNode::create();
    stencil->drawSolidCircle(Vec2(0.0,0.0), 35.0f, 0.0f, 100.0f, Color4F::WHITE);
    
    // create the clipping node and set the stencil
    auto clipper = ClippingNode::create();
    clipper->setStencil(stencil);
    clipper->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.75 + origin.y));
    clipper->addChild(profSprite);
    clipper->setColor(Color3B::WHITE);
    this->addChild(clipper);
    
    //create stress n social attribute
    Image * stressImage = new Image();
    stressImage->initWithImageFile("images/StressCircle.png");
    Texture2D * stressTexture = new Texture2D();
    stressTexture->initWithImage(stressImage);
    Sprite *stressSprite = Sprite::createWithTexture(stressTexture);
    stressSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.75 + origin.y));
    
    Image * socialImage = new Image();
    socialImage->initWithImageFile("images/SocialCircle.png");
    Texture2D * socialTexture = new Texture2D();
    socialTexture->initWithImage(socialImage);
    Sprite *socialSprite = Sprite::createWithTexture(socialTexture);
    socialSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.75 + origin.y));
    
    stressSprite->setRotation3D(Vec3(0,90,0));
    stressSprite->addChild(socialSprite);
    this->addChild(stressSprite);
    
    
}

void MainMenu::onSharedSuccess(const std::string &message)
{ cout<<"\nFBAPICALL(onSharedSuccess)"; }

void MainMenu::onSharedCancel()
{ cout<<"\nFBAPICALL(onSharedCancel)"; }

void MainMenu::onSharedFailed(const std::string &message)
{ cout<<"\nFBAPICALL(onSharedFailed)"; }

void MainMenu::onFetchFriends(bool ok, const std::string &msg)
{ cout<<"\nFBAPICALL(onFetchFriends)"; }

void MainMenu::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo &friends)
{ cout<<"\nFBAPICALL(onRequestInvitableFriends)"; }

void MainMenu::onInviteFriendsWithInviteIdsResult(bool result, const std::string &msg)
{ cout<<"\nFBAPICALL(onInviteFriendsWithInviteIdsResult)"; }

void MainMenu::onInviteFriendsResult(bool result, const std::string &msg)
{ cout<<"\nFBAPICALL(onInviteFriendsResult)"; }

void MainMenu::onGetUserInfo(const sdkbox::FBGraphUser &userInfo)
{ cout<<"\nFBAPICALL(onGetUserInfo)"; }

void MainMenu::onAskGiftResult(bool result, const std::string &msg)
{ cout<<"\nFBAPICALL(onAskGiftResult)"; }


