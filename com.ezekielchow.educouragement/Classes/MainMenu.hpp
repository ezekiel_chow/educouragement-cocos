//
//  MainMenu.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 08/03/2017.
//
//

#ifndef MainMenu_hpp
#define MainMenu_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "PluginFacebook/PluginFacebook.h"
#include "network/HttpClient.h"
#include "Shop.hpp"
#include "PlayerDataSingleton.hpp"
#include "Leaderboards.hpp"

class MainMenu : public cocos2d::LayerColor, public sdkbox::FacebookListener
{
private:
    PlayerDataSingleton mySingleton;
    cocos2d::ProgressTimer *expBar;
    
    //fbapi funcs
    virtual void onLogin(bool isLogin, const std::string& msg);
    virtual void onSharedSuccess(const std::string& message);
    virtual void onSharedFailed(const std::string& message);
    virtual void onSharedCancel();
    virtual void onAPI(const std::string& key, const std::string& jsonData);
    virtual void onPermission(bool isLogin, const std::string& msg);
    virtual void onFetchFriends(bool ok, const std::string& msg);
    virtual void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends );
    virtual void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg );
    virtual void onInviteFriendsResult( bool result, const std::string& msg );
    virtual void onGetUserInfo( const sdkbox::FBGraphUser& userInfo);
    virtual void onAskGiftResult(bool result, const std::string& msg);
    
    //background
    virtual void drawBackground();
    
    //image request
    virtual void checkFacebookPermissions();
    virtual void onRequestImgCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    
    //get player attributes
    virtual void checkIfNewPlayer(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    virtual void createAttributes(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    virtual void drawAttributes(int strength, int intelligence, int stress, int social, int level, int experience, std::string team, int gold);
    
    //bottom menu
    virtual void drawMenu();
    
    //navigation
    virtual void navigateTo(Ref * sender, cocos2d::Scene * scene);
    
    //info
    virtual void drawDialog(std::string message);
    
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(MainMenu);
};

#endif /* MainMenu_hpp */
