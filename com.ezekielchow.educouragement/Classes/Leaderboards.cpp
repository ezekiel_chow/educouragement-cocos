//
//  Leaderboards.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 25/03/2017.
//
//

#include "MainMenu.hpp"
#include "Leaderboards.hpp"
#include "external/rapidjson/reader.h"
#include "external/rapidjson/writer.h"
#include "external/rapidjson/document.h"
#include "external/rapidjson/stringbuffer.h"
USING_NS_CC;

using namespace rapidjson;

Scene* Leaderboards::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Leaderboards::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Leaderboards::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B::GRAY) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    mySingleton = PlayerDataSingleton();
    
    //back button
    auto backBtn = cocos2d::ui::Button::create("images/backarrowWhite.png", "images/backarrowWhite.png", "");
    backBtn->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.95 + origin.y));
    backBtn->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->reorderChild(backBtn, 10);
    this->addChild(backBtn);
    
    auto sceneTitle = Label::createWithTTF("Rankings" , "fonts/arial.ttf", 14);
    sceneTitle->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.95 + origin.y));
    this->addChild(sceneTitle);
    
    //gettting questions
    cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
    request->setUrl("https://educouragement-rest.herokuapp.com/api/winners");
    request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    request->setResponseCallback( CC_CALLBACK_2(Leaderboards::getRanking, this) );
    request->setTag("GET Rankings");
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
    
    return true;
}

void Leaderboards::getRanking(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        log("onHttpRequestCompleted - No Response");
        return;
    }
    
    if (!response->isSucceed())
    {
        log("onHttpRequestCompleted - Response failed");
        log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    char * concatenated = (char *) malloc(buffer->size() + 1);
    std::string s2(buffer->begin(), buffer->end());
    strcpy(concatenated, s2.c_str());
    
    Document document;
    document.Parse(concatenated);
    
    // 3. Stringify the DOM
    StringBuffer stringBuffer;
    Writer<rapidjson::StringBuffer> writer(stringBuffer);
    document.Accept(writer);
    
    assert(document.IsObject());
    assert(document["data"]["rowCount"].IsInt());
    
    if (document["data"]["rowCount"].GetInt() > 0) { //got questions
        assert(document["data"]["rows"].IsArray());
        
        std::string name;
        int correct_total;
        
        //iterate array
        for (rapidjson::Value::ConstValueIterator arrayitr = document["data"]["rows"].Begin(); arrayitr != document["data"]["rows"].End(); ++arrayitr)
        {
            //iterate one question details
            for (rapidjson::Value::ConstMemberIterator objitr = arrayitr->GetObject().MemberBegin();
                 objitr != arrayitr->GetObject().MemberEnd(); ++objitr)
            {
                
                if (strcmp(objitr->name.GetString(), "name") == 0) {
                    name = objitr->value.GetString();
                }
                else if (strcmp(objitr->name.GetString(), "correct_total") == 0) {
                    correct_total = objitr->value.GetInt();
                }
                else
                {
                    std::cout<<"throw away value";
                }
            }
            rankingPair.push_back(std::make_pair(name, correct_total));
        }
    }
    drawRanking();
}

void Leaderboards::drawRanking()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto Num = Label::createWithTTF("Num", "fonts/arial.ttf", 13);
    Num->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.85 + origin.y));
    Num->setTextColor(Color4B::WHITE);
    this->reorderChild(Num, 5);
    this->addChild(Num);
    
    auto Player = Label::createWithTTF("Player Name", "fonts/arial.ttf", 13);
    Player->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*0.85 + origin.y));
    Player->setTextColor(Color4B::WHITE);
    this->reorderChild(Player, 5);
    this->addChild(Player);
    
    auto Correct = Label::createWithTTF("Points", "fonts/arial.ttf", 13);
    Correct->setPosition(Vec2(visibleSize.width*0.85 + origin.x, visibleSize.height*0.85 + origin.y));
    Correct->setTextColor(Color4B::WHITE);
    this->reorderChild(Correct, 5);
    this->addChild(Correct);
    
    float height = 0.8;
    int counter = 1;
    for (std::vector<std::pair<std::string, int>>::iterator it = rankingPair.begin() ; it != rankingPair.end(); ++it)
    {
        std::pair<std::string, int> onePair = *it;
        std::cout<<"\npair?"<<std::get<0>(onePair)<<" "<<std::get<1>(onePair);
        
        auto Num = Label::createWithTTF(std::to_string(counter) + ".", "fonts/arial.ttf", 16);
        Num->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*height + origin.y));
        Num->setTextColor(Color4B::WHITE);
        this->reorderChild(Num, 5);
        this->addChild(Num);
        
        auto Player = Label::createWithTTF(std::get<0>(onePair), "fonts/arial.ttf", 16);
        Player->setPosition(Vec2(visibleSize.width*0.5 + origin.x, visibleSize.height*height + origin.y));
        Player->setTextColor(Color4B::WHITE);
        this->reorderChild(Player, 5);
        this->addChild(Player);
        
        auto Correct = Label::createWithTTF(std::to_string(std::get<1>(onePair)), "fonts/arial.ttf", 16);
        Correct->setPosition(Vec2(visibleSize.width*0.85 + origin.x, visibleSize.height*height + origin.y));
        Correct->setTextColor(Color4B::WHITE);
        this->reorderChild(Correct, 5);
        this->addChild(Correct);
        counter++;
        height = height - 0.05;
    }
}
