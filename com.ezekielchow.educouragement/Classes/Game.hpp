//
//  Game.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 12/03/2017.
//
//

#ifndef Game_hpp
#define Game_hpp

#include <stdio.h>
#include "PlayerDataSingleton.hpp"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"
#include "QuestionsModel.hpp"
#include "EndGame.hpp"

class Game : public cocos2d::LayerColor
{
private:
    
    PlayerDataSingleton mySingleton;
    std::string topicSelected;
    std::vector<QuestionSet> allQuestions;
    int questionCounter, currentCorrectAnswer, selectedLevel, life, skill, correctAnsCount, wrongAnsCount;
    float skillPercentageForOneSecond, lifeConvertedToPercentage;
    cocos2d::ui::Text *question;
    cocos2d::ui::Button *answer1, *answer2, *answer3, *answer4;
    cocos2d::ProgressTimer *lifeBar, *skillBar;
    
    virtual void updateGame(float delta);
    virtual void drawMainGame();
    virtual void getGameData();
    virtual void getQuestions(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    virtual void loadQuestions(int questionNum);
    virtual void animateCorrect(cocos2d::Vec2 buttonPosition, float barPercentage);
    virtual void animateWrong(cocos2d::Vec2 buttonPosition);
    
    //after loading complete
    virtual void startGame();
    virtual bool checkAnswer(int playerAnswer);
    
    //gameplay
    virtual bool skillBarEmpty();
    virtual bool lifeBarEmpty();
    
    virtual void loadEndingScreen();
    
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(Game);
};
#endif /* Game_hpp */
