//
//  Settings.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 28/03/2017.
//
//

#ifndef Settings_hpp
#define Settings_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"
#include "PlayerDataSingleton.hpp"
#include "PluginFacebook/PluginFacebook.h"

class Settings : public cocos2d::LayerColor, sdkbox::FacebookListener
{
private:
    virtual void onLogin(bool isLogin, const std::string& msg);
    virtual void onSharedSuccess(const std::string& message);
    virtual void onSharedFailed(const std::string& message);
    virtual void onSharedCancel();
    virtual void onAPI(const std::string& key, const std::string& jsonData);
    virtual void onPermission(bool isLogin, const std::string& msg);
    virtual void onFetchFriends(bool ok, const std::string& msg);
    virtual void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends );
    virtual void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg );
    virtual void onInviteFriendsResult( bool result, const std::string& msg );
    virtual void onGetUserInfo( const sdkbox::FBGraphUser& userInfo);
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(Settings);
};

#endif /* Settings_hpp */
