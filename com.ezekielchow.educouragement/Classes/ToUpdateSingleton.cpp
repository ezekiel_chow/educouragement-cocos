//
//  ToUpdateSingleton.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 20/03/2017.
//
//

#include "ToUpdateSingleton.hpp"

// this goes in the cpp file
struct ToUpdateSingleton::impl
{
    std::string teamR = "", roomNameR = "";
    int strengthR = 0, intelligenceR = 0, stressR = 0, socialR = 0, levelR = 0, experienceR = 0, goldR = 0, selectedQuestR, correctTotalR, wrongTotalR;
    bool didSucceedR = false;
    
    //setters
    void setStrength(int strength)
    {
        strengthR = strength;
    }
    
    void setIntelligence(int intelligence)
    {
        intelligenceR = intelligence;
    }
    
    void setLevel(int level)
    {
        levelR = level;
    }
    
    void setExperience(int experience)
    {
        experienceR = experience;
    }
    
    void setGold(int gold)
    {
        goldR = gold;
    }
    
    //getters
    int getStrength()
    {
        return strengthR;
    }
    
    int getIntelligence()
    {
        return intelligenceR;
    }

    int getLevel()
    {
        return levelR;
    }
    
    int getExperience()
    {
        return experienceR;
    }
    
    int getGold()
    {
        return goldR;
    }
};

// implement public interface in terms of private impl
//getters
int ToUpdateSingleton::getStrength()
{
    return get_impl().getStrength();
}

int ToUpdateSingleton::getIntelligence()
{
    return get_impl().getIntelligence();
}

int ToUpdateSingleton::getLevel()
{
    return get_impl().getLevel();
}

int ToUpdateSingleton::getExperience()
{
    return get_impl().getExperience();
}

int ToUpdateSingleton::getGold()
{
    return get_impl().getGold();
}

//setters

void ToUpdateSingleton::setStrength(int strength)
{
    return get_impl().setStrength(strength);
}

void ToUpdateSingleton::setIntelligence(int intelligence)
{
    return get_impl().setIntelligence(intelligence);
}

void ToUpdateSingleton::setLevel(int level)
{
    return get_impl().setLevel(level);
}

void ToUpdateSingleton::setExperience(int experience)
{
    return get_impl().setExperience(experience);
}

void ToUpdateSingleton::setGold(int gold)
{
    return get_impl().setGold(gold);
}

ToUpdateSingleton::impl& ToUpdateSingleton::get_impl()
{
    // create first time code flows over it - c++ standard
    // thread safe in c++11 - guarantee
    static impl instance {};
    return instance;
}
