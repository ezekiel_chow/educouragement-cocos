//
//  QuestSelect.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 17/03/2017.
//
//

#include "QuestSelect.hpp"
USING_NS_CC;

Scene* QuestSelect::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = QuestSelect::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool QuestSelect::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    mySingleton = PlayerDataSingleton();
    mySingleton.setQuestSelected(1);
    
    Sprite * space = Sprite::create("images/Space.jpg");
    Size wSize = Size(Director::getInstance()->getWinSizeInPixels().width, Director::getInstance()->getWinSizeInPixels().height);
    Size scrSize= Size(1136, 640);
    space->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    space->setScaleX(wSize.width/scrSize.width);
    space->setScaleY(wSize.height/scrSize.height);
    this->addChild(space);
    this->reorderChild(space, -2);
    
    auto sceneTitle = Label::createWithTTF("Quest Menu" , "fonts/arial.ttf", 14);
    sceneTitle->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.95 + origin.y));
    this->addChild(sceneTitle);
    
    Sprite *container = Sprite::create("images/QuestContainer.png");
    container->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(container);
    this->reorderChild(container, -1);
    
    //back button
    auto backBtn = cocos2d::ui::Button::create("images/backarrowWhite.png", "images/backarrowWhite.png", "");
    backBtn->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height*0.95 + origin.y));
    backBtn->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->replaceScene(MainMenu::createScene());
                break;
            default:
                break;
        }
    });
    this->reorderChild(backBtn, 10);
    this->addChild(backBtn);
    
    drawQuestElements("images/spaceship.png", "BUILDING YOUR SPACESHIP", "G+150 EXP+80" , "HTML", "Easy");
    
    //draw next and previous
    nextButton = cocos2d::ui::Button::create("images/NextButton.png", "images/NextButton.png", "");
    nextButton->setPosition(Vec2(visibleSize.width*0.9 + origin.x, visibleSize.height/2 + origin.y));
    nextButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                this->updateQuestElements("images/Resources.png", "RESOURCES FOR JOURNEY", "G+300 EXP+300" , "CSS", "Medium");
                startMission->setEnabled(false);
                startMission->setOpacity(0);
                nextButton->setEnabled(false);
                nextButton->setOpacity(0);
                previousButton->setEnabled(true);
                previousButton->setOpacity(255);
                mySingleton.setQuestSelected(2);
                break;
            default:
                break;
        }
    });
    this->reorderChild(nextButton, 10);
    nextButton->setEnabled(true);
    nextButton->setOpacity(255);
    this->addChild(nextButton);
    
    previousButton = cocos2d::ui::Button::create("images/PreviousButton.png", "images/PreviousButton.png", "");
    previousButton->setPosition(Vec2(visibleSize.width*0.1 + origin.x, visibleSize.height/2 + origin.y));
    previousButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                this->updateQuestElements("images/spaceship.png", "BUILDING YOUR SPACESHIP", "G+150 EXP+80" , "HTML", "Easy");
                startMission->setEnabled(true);
                startMission->setOpacity(255);
                previousButton->setEnabled(false);
                previousButton->setOpacity(0);
                nextButton->setEnabled(true);
                nextButton->setOpacity(255);
                mySingleton.setQuestSelected(1);
                break;
            default:
                break;
        }
    });
    previousButton->setEnabled(false);
    previousButton->setOpacity(0);
    this->reorderChild(previousButton, 10);
    this->addChild(previousButton);
    
    return true;
}

void QuestSelect::drawQuestElements(std::string imageName, std::string questTitle, std::string rewards, std::string type, std::string difficulty)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    imageSprite = Sprite::create(imageName);
    imageSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.67 + origin.y));
    this->reorderChild(imageSprite, 4);
    this->addChild(imageSprite);
    
    titlelbl = Label::createWithTTF(questTitle, "fonts/arial.ttf", 8);
    titlelbl->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.5 + origin.y));
    titlelbl->setTextColor(Color4B::BLUE);
    this->reorderChild(titlelbl, 5);
    this->addChild(titlelbl);
    
    //static
    auto rewardsStatic = Label::createWithTTF("Rewards:", "fonts/arial.ttf", 10);
    rewardsStatic->setPosition(Vec2(visibleSize.width*0.35 + origin.x, visibleSize.height*0.45 + origin.y));
    rewardsStatic->setTextColor(Color4B::BLACK);
    this->reorderChild(rewardsStatic, 5);
    this->addChild(rewardsStatic);
    
    auto typeStatic = Label::createWithTTF("Type:", "fonts/arial.ttf", 11);
    typeStatic->setPosition(Vec2(visibleSize.width*0.3 + origin.x, visibleSize.height*0.40 + origin.y));
    typeStatic->setTextColor(Color4B::BLACK);
    this->reorderChild(typeStatic, 5);
    this->addChild(typeStatic);
    
    auto difficultyStatic = Label::createWithTTF("Difficulty:", "fonts/arial.ttf", 11);
    difficultyStatic->setPosition(Vec2(visibleSize.width*0.35 + origin.x, visibleSize.height*0.35 + origin.y));
    difficultyStatic->setTextColor(Color4B::RED);
    this->reorderChild(difficultyStatic, 5);
    this->addChild(difficultyStatic);
    
    rewardslbl = Label::createWithTTF(rewards, "fonts/arial.ttf", 9);
    rewardslbl->setPosition(Vec2(visibleSize.width*0.65 + origin.x, visibleSize.height*0.45 + origin.y));
    rewardslbl->setTextColor(Color4B::BLACK);
    this->reorderChild(rewardslbl, 5);
    this->addChild(rewardslbl);
    
    typelbl = Label::createWithTTF(type, "fonts/arial.ttf", 11);
    typelbl->setPosition(Vec2(visibleSize.width*0.6 + origin.x, visibleSize.height*0.40 + origin.y));
    typelbl->setTextColor(Color4B::BLACK);
    this->reorderChild(typelbl, 5);
    this->addChild(typelbl);
    
    difficultylbl = Label::createWithTTF(difficulty, "fonts/arial.ttf", 11);
    difficultylbl->setPosition(Vec2(visibleSize.width*0.6 + origin.x, visibleSize.height*0.35 + origin.y));
    difficultylbl->setTextColor(Color4B::RED);
    this->reorderChild(difficultylbl, 5);
    this->addChild(difficultylbl);
    
    //play button
    startMission = cocos2d::ui::Button::create("images/StartMissionBefore.png", "images/StartMissionAfter.png", "");
    startMission->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height*0.25 + origin.y));
    startMission->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                std::cout<<"\nquest:"<<mySingleton.getQuestSelected();
                Director::getInstance()->replaceScene(Game::createScene());
                break;
            default:
                break;
        }
    });
    this->reorderChild(startMission, 5);
    this->addChild(startMission);
}

void QuestSelect::updateQuestElements(std::string imageName, std::string questTitle, std::string rewards, std::string type, std::string difficulty)
{
    imageSprite->setTexture(imageName);
    titlelbl->setString(questTitle);
    rewardslbl->setString(rewards);
    typelbl->setString(type);
    difficultylbl->setString(difficulty);
}
