//
//  PlayerDataSingleton.cpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 10/03/2017.
//
//

#include "PlayerDataSingleton.hpp"

// this goes in the cpp file
struct PlayerDataSingleton::impl
{
    std::string teamR = "", roomNameR = "";
    int strengthR = 0, intelligenceR = 0, stressR = 0, socialR = 0, levelR = 0, experienceR = 0, goldR = 0, selectedQuestR, correctTotalR, wrongTotalR;
    std::vector<int> itemsR;
    bool didSucceedR = false, didWinR = false;
    
    //setters
    void setTeam(std::string team)
    {
        teamR = team;
    }
    
    void setStrength(int strength)
    {
        strengthR = strength;
    }
    
    void setIntelligence(int intelligence)
    {
        intelligenceR = intelligence;
    }
    
    void setStress(int stress)
    {
        stressR = stress;
    }
    
    void setSocial(int social)
    {
        socialR = social;
    }
    
    void setLevel(int level)
    {
        levelR = level;
    }
    
    void setExperience(int experience)
    {
        experienceR = experience;
    }
    
    void setGold(int gold)
    {
        goldR = gold;
    }
    
    void setRoomName(std::string roomName)
    {
        roomNameR = roomName;
    }
    
    void setQuestSelected(int questSelected)
    {
        selectedQuestR = questSelected;
    }
    
    void setCorrectTotal(int correctTotal)
    {
        correctTotalR = correctTotal;
    }
    
    void setMissionSucceed(bool didSucceed)
    {
        didSucceedR = didSucceed;
    }
    
    void setWrongTotal(int wrongTotal)
    {
        wrongTotalR = wrongTotal;
    }
    
    void setItems(std::vector<int> items)
    {
        itemsR = items;
    }
    
    void setDidWin(bool didWin)
    {
        didWinR = didWin;
    }
    
    //getters
    std::string getTeam()
    {
        return teamR;
    }
    
    int getStrength()
    {
        return strengthR;
    }
    
    int getIntelligence()
    {
        return intelligenceR;
    }
    
    int getStress()
    {
        return stressR;
    }
    
    int getSocial()
    {
        return socialR;
    }
    
    int getLevel()
    {
        return levelR;
    }
    
    int getExperience()
    {
        return experienceR;
    }
    
    int getGold()
    {
        return goldR;
    }
    std::string getRoomName()
    {
        return roomNameR;
    }
    
    int getSelectedQuest()
    {
        return selectedQuestR;
    }
    
    int getCorrectTotal()
    {
        return correctTotalR;
    }
    
    bool getMissionSucceed()
    {
        return didSucceedR;
    }
    
    int getWrongTotal()
    {
        return wrongTotalR;
    }
    
    std::vector<int> getItems()
    {
        return itemsR;
    }
    
    bool getDidWin()
    {
        return didWinR;
    }
};

// implement public interface in terms of private impl
//getters
std::string PlayerDataSingleton::getTeam()
{
    return get_impl().getTeam();
}

int PlayerDataSingleton::getStrength()
{
    return get_impl().getStrength();
}

int PlayerDataSingleton::getIntelligence()
{
    return get_impl().getIntelligence();
}

int PlayerDataSingleton::getStress()
{
    return get_impl().getStress();
}

int PlayerDataSingleton::getSocial()
{
    return get_impl().getSocial();
}

int PlayerDataSingleton::getLevel()
{
    return get_impl().getLevel();
}

int PlayerDataSingleton::getExperience()
{
    return get_impl().getExperience();
}

int PlayerDataSingleton::getGold()
{
    return get_impl().getGold();
}

std::string PlayerDataSingleton::getRoomName()
{
    return get_impl().getRoomName();
}

int PlayerDataSingleton::getQuestSelected()
{
    return get_impl().getSelectedQuest();
}

int PlayerDataSingleton::getCorrectTotal()
{
    return get_impl().getCorrectTotal();
}

bool PlayerDataSingleton::getMissionSucceed()
{
    return get_impl().getMissionSucceed();
}

int PlayerDataSingleton::getWrongTotal()
{
    return get_impl().getWrongTotal();
}

std::vector<int> PlayerDataSingleton::getItems()
{
    return get_impl().getItems();
}

bool PlayerDataSingleton::getDidWin()
{
    return get_impl().getDidWin();
}

//setters
void PlayerDataSingleton::setTeam(std::string team)
{
    return get_impl().setTeam(team);
}

void PlayerDataSingleton::setStrength(int strength)
{
    return get_impl().setStrength(strength);
}

void PlayerDataSingleton::setIntelligence(int intelligence)
{
    return get_impl().setIntelligence(intelligence);
}

void PlayerDataSingleton::setStress(int stress)
{
    return get_impl().setStress(stress);
}

void PlayerDataSingleton::setSocial(int social)
{
    return get_impl().setSocial(social);
}

void PlayerDataSingleton::setLevel(int level)
{
    return get_impl().setLevel(level);
}

void PlayerDataSingleton::setExperience(int experience)
{
    return get_impl().setExperience(experience);
}

void PlayerDataSingleton::setGold(int gold)
{
    return get_impl().setGold(gold);
}

void PlayerDataSingleton::setRoomName(std::string roomName)
{
    return get_impl().setRoomName(roomName);
}

void PlayerDataSingleton::setQuestSelected(int questSelected)
{
    return get_impl().setQuestSelected(questSelected);
}

void PlayerDataSingleton::setCorrectTotal(int correctTotal)
{
    return get_impl().setCorrectTotal(correctTotal);
}

void PlayerDataSingleton::setMissonSucceed(bool didSucceed)
{
    return get_impl().setMissionSucceed(didSucceed);
}

void PlayerDataSingleton::setWrongTotal(int wrongTotal)
{
    return get_impl().setWrongTotal(wrongTotal);
}

void PlayerDataSingleton::setItems(std::vector<int> items)
{
    return get_impl().setItems(items);
}

void PlayerDataSingleton::setDidWin(bool didWin)
{
    return get_impl().setDidWin(didWin);
}

PlayerDataSingleton::impl& PlayerDataSingleton::get_impl()
{
    // create first time code flows over it - c++ standard
    // thread safe in c++11 - guarantee
    static impl instance {};
    return instance;
}
