//
//  QuestSelect.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 17/03/2017.
//
//

#ifndef QuestSelect_hpp
#define QuestSelect_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PlayerDataSingleton.hpp"
#include "Game.hpp"

class QuestSelect : public cocos2d::Layer
{
private:
    cocos2d::Sprite *imageSprite;
    cocos2d::Label *titlelbl, *rewardslbl, *typelbl, *difficultylbl;
    cocos2d::ui::Button *previousButton, *nextButton, *startMission;
    PlayerDataSingleton mySingleton;
    
    virtual void drawQuestElements(std::string imageName, std::string questTitle, std::string rewards, std::string type, std::string difficulty);
    
    virtual void updateQuestElements(std::string imageName, std::string questTitle, std::string rewards, std::string type, std::string difficulty);
    
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(QuestSelect);
};

#endif /* QuestSelect_hpp */
