//
//  Leaderboards.hpp
//  com.ezekielchow.educouragement
//
//  Created by Ezekiel Chow on 25/03/2017.
//
//

#ifndef Leaderboards_hpp
#define Leaderboards_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"
#include "PlayerDataSingleton.hpp"

class Leaderboards : public cocos2d::LayerColor
{
private:
    PlayerDataSingleton mySingleton;
    std::vector<std::pair<std::string, int>> rankingPair{};
    
    virtual void getRanking(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    virtual void drawRanking();
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(Leaderboards);
};

#endif /* Leaderboards_hpp */
